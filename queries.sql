--Creating new user and new database for homework task.

CREATE USER homeworker WITH PASSWORD 'homeworker01';

CREATE DATABASE homework OWNER homeworker;

--Connecting with database

psql -h localhost -p 5432 -U homeworker -W homework

--zad1

CREATE TABLE dragons (
	id SERIAL PRIMARY KEY,
	name VARCHAR (256),
	color VARCHAR (256),
	wingspan int);

--zad2

CREATE TABLE eggs (
	id SERIAL PRIMARY KEY,
	weight int,
	diameter int,
	id_dragon int,
	FOREIGN KEY (id_dragon) REFERENCES dragons (id));

--zad3

CREATE TABLE ornaments (
	id_egg int,
	color VARCHAR (256),
	pattern VARCHAR (256),
	PRIMARY KEY (id_egg),
	FOREIGN KEY (id_egg) REFERENCES eggs (id));


--zad4

CREATE TABLE lands (
	id SERIAL PRIMARY KEY,
	name VARCHAR (256));

--Creating intermediary table for many to many relation

CREATE TABLE dragons_lands (
	id_dragon int,
	id_land int,
	PRIMARY KEY (id_dragon,id_land),
	FOREIGN KEY (id_dragon) REFERENCES dragons (id),
	FOREIGN KEY (id_land) REFERENCES lands (id));

	zad5

--dragons table
INSERT INTO dragons (name, color, wingspan) VALUES 
	('Dygir', 'green', 200),
	('Bondril', 'red', 100),
	('Onosse', 'black', 250),
	('Chiri', 'yellow', 50),
	('Lase', 'blue', 300);

--eggs table
INSERT INTO eggs (weight, diameter, id_dragon) VALUES
	(300, 20, (SELECT id FROM dragons WHERE name='Dygir')),
	(340, 30, (SELECT id FROM dragons WHERE name='Dygir')),
	(200, 10, (SELECT id FROM dragons WHERE name='Bondril')),
	(230, 20, (SELECT id FROM dragons WHERE name='Onosse')),
	(300, 25, (SELECT id FROM dragons WHERE name='Onosse')),
	(200, 15, (SELECT id FROM dragons WHERE name='Onosse'));

--ornaments table
INSERT INTO ornaments (id_egg, color, pattern) VALUES
	((SELECT id FROM eggs WHERE weight=300 and diameter=20), 'pink', 'mesh'),
	((SELECT id FROM eggs WHERE weight=340 and diameter=30), 'black', 'striped'),
	((SELECT id FROM eggs WHERE weight=200 and diameter=10), 'yellow', 'dotted'),
	((SELECT id FROM eggs WHERE weight=230 and diameter=20), 'blue', 'dotted'),
	((SELECT id FROM eggs WHERE weight=300 and diameter=25), 'green', 'striped'),
	((SELECT id FROM eggs WHERE weight=200 and diameter=15), 'red', 'mesh');

--lands table
INSERT INTO lands (name) VALUES ('Froze'), ('Oswia'), ('Oscyae'), ('Oclurg');

--dragons_lands table (intermediary table for many to many relation)
INSERT INTO dragons_lands (id_dragon, id_land) VALUES 
	((SELECT id FROM dragons WHERE name='Dygir'), (SELECT id FROM lands WHERE name='Froze')),
	((SELECT id FROM dragons WHERE name='Bondril'), (SELECT id FROM lands WHERE name='Froze')),
	((SELECT id FROM dragons WHERE name='Dygir'), (SELECT id FROM lands WHERE name='Oswia')),
	((SELECT id FROM dragons WHERE name='Onosse'), (SELECT id FROM lands WHERE name='Oswia')),
	((SELECT id FROM dragons WHERE name='Chiri'), (SELECT id FROM lands WHERE name='Oswia'));

--zad6

CREATE VIEW eggs_ornaments 
	AS SELECT diameter, weight, color, pattern 
	FROM eggs 
	INNER JOIN ornaments 
	ON eggs.id = ornaments.id_egg;

--zad7 (Changed name of view because of name duplicated)

CREATE VIEW dragons_lands_view (dragon_name, land_name) 
	AS SELECT dragons.name, lands.name 
	FROM dragons_lands 
	INNER JOIN dragons ON dragons_lands.id_dragon = dragons.id 
	INNER JOIN lands ON dragons_lands.id_land = lands.id;

--zad8

CREATE VIEW dragons_eggs (dragon_name, egg_weight, egg_diameter) 
	AS SELECT dragons.name, eggs.weight, eggs.diameter 
	FROM dragons 
	INNER JOIN eggs 
	ON dragons.id = eggs.id_dragon; 

--zad9

CREATE VIEW eggless_dragons (dragon_name) 
	AS SELECT dragons.name 
	FROM dragons 
	LEFT JOIN eggs 
	ON dragons.id = eggs.id_dragon 
	WHERE id_dragon IS null;

--zad10

SELECT * FROM dragons;

 id |  name   | color  | wingspan 
----+---------+--------+----------
  1 | Dygir   | green  |      200
  2 | Bondril | red    |      100
  3 | Onosse  | black  |      250
  4 | Chiri   | yellow |       50
  5 | Lase    | blue   |      300

SELECT * FROM eggs;

 id | weight | diameter | id_dragon 
----+--------+----------+-----------
  1 |    300 |       20 |         1
  2 |    340 |       30 |         1
  3 |    200 |       10 |         2
  4 |    230 |       20 |         3
  5 |    300 |       25 |         3
  6 |    200 |       15 |         3

SELECT * FROM ornaments;

 id_egg | color  | pattern 
--------+--------+---------
      1 | pink   | mesh
      2 | black  | striped
      3 | yellow | dotted
      4 | blue   | dotted
      5 | green  | striped
      6 | red    | mesh

SELECT * FROM lands;

 id |  name  
----+--------
  1 | Froze
  2 | Oswia
  3 | Oscyae
  4 | Oclurg

SELECT * FROM dragons_lands;

 id_dragon | id_land 
-----------+---------
         1 |       1
         2 |       1
         1 |       2
         3 |       2
         4 |       2

SELECT * FROM eggs_ornaments;

 diameter | weight | color  | pattern 
----------+--------+--------+---------
       20 |    300 | pink   | mesh
       30 |    340 | black  | striped
       10 |    200 | yellow | dotted
       20 |    230 | blue   | dotted
       25 |    300 | green  | striped
       15 |    200 | red    | mesh

SELECT * FROM dragons_lands_view;

 dragon_name | land_name 
-------------+-----------
 Dygir       | Froze
 Bondril     | Froze
 Dygir       | Oswia
 Onosse      | Oswia
 Chiri       | Oswia

SELECT * FROM dragons_eggs;

 dragon_name | egg_weight | egg_diameter 
-------------+------------+--------------
 Dygir       |        300 |           20
 Dygir       |        340 |           30
 Bondril     |        200 |           10
 Onosse      |        230 |           20
 Onosse      |        300 |           25
 Onosse      |        200 |           15

SELECT * FROM eggless_dragons;

 dragon_name 
-------------
 Chiri
 Lase

--zad11

SELECT name FROM dragons WHERE dragons.wingspan>=200 AND dragons.wingspan<=400;

  name  
--------
 Dygir
 Onosse
 Lase

--zad12

DROP TABLE dragons, eggs, ornaments, lands, dragons_lands CASCADE;




